// теоретический вопрос
//ответ : циклы нужны для того что бы не повторять какое то действие много раз,
// а записать это действие в цикл(в прау строк) и уменьшить обьем кода


let userNumber = 0;
do {
    userNumber = parseInt(prompt("Enter number"))
} while (!Number.isInteger(userNumber));

if (userNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 5; i <= userNumber; i++) {
        if (i % 5 == 0)
            console.log(i);
    }
}
