function calc(num1, num2, operation) {
    switch (operation) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "/":
            return num1 / num2;
        case "*":
            return num1 * num2;
        default:
            return 0;
    }
}

function createCalk() {
    while (confirm('Start the calculator?')) {
        let num1 = '';
        let num2 = '';
        do {
            num1 = prompt('Enter first number', num1);
            num2 = prompt('Enter second number', num2);
        } while (isNaN(num1) || isNaN(num2));
        const operation = prompt('Which operation to perform?');
        num1 = Number(num1);
        num2 = Number(num2);
        console.log(calc(num1, num2, operation));
    }
}

createCalk();
