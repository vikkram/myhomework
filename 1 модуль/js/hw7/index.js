function creatList(arr, parent = document.body) {
    let ul = document.createElement('ul');
    let li = arr.map((el) => {
        return `<li>${el}</li>`
    });
    li.forEach(el => {
        ul.insertAdjacentHTML('beforeend', el)
    });
    if (parent === document.body) {
        parent.prepend(ul);
    } else {
        parent.append(ul);
    }
}

const arr = ['Kiev', 'Kharkov', 'Odessa', 'Lvov', 'hello', 'world'];

creatList(arr, document.querySelector('.test-list'));