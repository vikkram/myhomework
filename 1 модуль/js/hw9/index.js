
let tabs = document.querySelector('.tabs');
let tabsTitles = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelectorAll('.tabs-content li');

function hideAll () {
    tabsContent.forEach((el) => {
        el.style.display = 'none';
    });
};

for (let i = 0; i < tabsTitles.length; i++) {
    tabsTitles[i].setAttribute(`id`, `${i}tabsTitle`);
    tabsContent[i].setAttribute(`id`, `${i}tabsContent`);
}

tabs.addEventListener('click', (ev) => {
    hideAll();
    let num = parseInt(ev.target.id);
    tabsContent.forEach((el) => {
        if(el.id.startsWith(num)) {
            el.style.display = 'block';
        }
    });
});

hideAll();
