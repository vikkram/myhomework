function filterByFirst(arr, dataType) {
    arrReturn = []
    arr.forEach(el => {
        if(typeof(el) !== dataType){
            arrReturn.push(el)
        }
    })
    return arrReturn
}

// function filterBySecond(arr, dataType) {
//     return arr.filter(el => typeof(el) !== dataType)
// }

console.log(filterByFirst(['hello', 'object', null, '35', 35], 'string'))
// console.log(filterBySecond(['hello', 'object', null, '35', 35], 'string'));