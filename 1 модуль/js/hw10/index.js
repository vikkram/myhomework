const enterIconPassword = document.querySelector('.fas.fa-eye.icon-password');
const confirmIconPassword = document.querySelector('.fas.fa-eye.icon-password.confirm');
const enterPassword = document.querySelector('.enter-password');
const confirmPassword = document.querySelector('.confirm-password');
const confirmPassBtn = document.querySelector('.btn');
const passForm = document.querySelector('.password-form');

const showPass = function (showHidePass) {
    let inputWrapPass = showHidePass.closest('.input-wrapper').querySelector('input');
    if (inputWrapPass.type === 'password') {
        inputWrapPass.setAttribute('type', 'text');
        showHidePass.className = 'fas fa-eye-slash icon-password';
    } else {
        inputWrapPass.setAttribute('type', 'password');
        showHidePass.className = 'fas fa-eye icon-password';
    }
};

enterIconPassword.addEventListener('click', () => {
    showPass(enterIconPassword);
});

confirmIconPassword.addEventListener('click', () => {
    showPass(confirmIconPassword);
});

passForm.addEventListener('submit', (ev) => {
    ev.preventDefault();
    let invalidValue = document.querySelector('.invalid-value');

    if (enterPassword.value.length > 0 && enterPassword.value === confirmPassword.value) {
        if (invalidValue.classList.contains('active')) {
            invalidValue.classList.remove('active');
        }
        enterPassword.value = '';
        confirmPassword.value = '';
        alert('You are welcome');
    }else{
        if (!invalidValue.classList.contains('active')){
            invalidValue.classList.add('active');
        }
    }
});
