const creatDiv = document.createElement('div');
creatDiv.classList.add('price');
const inPut =document.createElement('input');
const cancelBtn = document.createElement('button');
cancelBtn.textContent = 'x';
const spanPrice = document.createElement('span');
spanPrice.classList.add('span-price');
const priceTag = document.createElement('span');
const priceIncorrect = document.createElement('span');
priceIncorrect.classList.add('price-incorrect');
priceTag.textContent = 'Price';
creatDiv.append(priceTag, inPut);
document.body.prepend(creatDiv);

inPut.addEventListener('focus', () => {
    inPut.classList.add('.focus');
    inPut.style.color = 'black';
    if (document.querySelector('.span-price') !== null){
        spanPrice.remove();
    }
    if (document.querySelector('.price-incorrect') !== null) {
        priceIncorrect.remove();
    }
});

inPut.addEventListener('focusout', () => {
    inPut.classList.remove('.focus');
    let value = inPut.value;

    if (value > 0) {
        spanPrice.textContent = `Текущая цена: ${value}`;
        spanPrice.append(cancelBtn);
        creatDiv.before(spanPrice);
        inPut.style.color = 'green';
    } else {
        inPut.style.color = 'red';
        priceIncorrect.textContent = 'Please enter correct price';
        creatDiv.after(priceIncorrect);
    }
});

cancelBtn.addEventListener('click', () => {
    inPut.value = '';
    inPut.style.color = 'black';
    spanPrice.remove();
});
