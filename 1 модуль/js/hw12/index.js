const imgSet = ['img/1.jpg', 'img/2.jpg', 'img/3.JPG', 'img/4.png'];
const imgToShow = document.querySelector('.image-to-show');
const pauseBtn = document.querySelector('.pause');
const continueBtn = document.querySelector('.continue');
let imgIndex = 0;
let paused = false;

const sliderFunc = function () {
    if (imgIndex === imgSet.length - 1) {
        imgIndex = 0;
    } else {
        imgIndex++;
    }
    imgToShow.setAttribute('src', imgSet[imgIndex]);
};

let slidInterval = setInterval(sliderFunc, 3000);

pauseBtn.addEventListener('click', () => {
    if (!paused) {
        clearInterval(slidInterval);
        paused = true;
    }
});

continueBtn.addEventListener('click', () => {
    if (paused) {
        slidInterval = setInterval(sliderFunc, 3000);
        paused = false;
    }
});
